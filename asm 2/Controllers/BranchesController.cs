﻿using asm_2.Models;
using Microsoft.AspNetCore.Mvc;
namespace asm_2.Controllers;

public class BranchesController : Controller
{
    private readonly List<Branch> _branches = new()
    {
        new Branch
        {
            BranchId = 1,
            Name = "Chi nhánh A",
            Address = "123 Đường ABC",
            City = "Thành phố A",
            State = "Active",
            ZipCode = "12345"
        },
       
    }; 


    public IActionResult Index()
    {

        return View("Branches", _branches);
    }

    public IActionResult Add()
    {
       
        return View("Branches", _branches);
    }

    [HttpPost]
    public IActionResult Add(Branch branch)
    {
        // Xử lý thêm chi nhánh vào danh sách hoặc cơ sở dữ liệu
        _branches.Add(branch);
        return RedirectToAction("Index");
    }

    public IActionResult Edit(int id)
    {
        // Tìm chi nhánh theo id và hiển thị giao diện sửa
        var branch = _branches.FirstOrDefault(b => b.BranchId == id);
        if (branch == null)
        {
            return NotFound();
        }
        return View("Branches", _branches);
    }

    [HttpPost]
    public IActionResult Edit(Branch branch)
    {
        // Xử lý cập nhật thông tin chi nhánh
        var existingBranch = _branches.FirstOrDefault(b => b.BranchId == branch.BranchId);
        if (existingBranch == null)
        {
            return NotFound();
        }
        existingBranch.Name = branch.Name;
        existingBranch.Address = branch.Address;
        existingBranch.City = branch.City;
        existingBranch.State = branch.State;
        existingBranch.ZipCode = branch.ZipCode;
        return RedirectToAction("Index");
    }

    public IActionResult Details(int id)
    {
        // Tìm chi nhánh theo id và hiển thị chi tiết
        var branch = _branches.FirstOrDefault(b => b.BranchId == id);
        if (branch == null)
        {
            return NotFound();
        }
        return View("Branches", _branches);
    }

    [HttpGet]
    public IActionResult Get()
    {
        // Trả về danh sách chi nhánh dưới dạng JSON (API)
        return Json(_branches);
    }

    [HttpPost]
    public IActionResult Remove(int id)
    {
        // Xóa chi nhánh theo id
        var branch = _branches.FirstOrDefault(b => b.BranchId == id);
        if (branch == null)
        {
            return NotFound();
        }
        _branches.Remove(branch);
        return RedirectToAction("Index");
    }
}